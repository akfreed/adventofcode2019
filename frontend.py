from abc import ABCMeta, abstractmethod


# ===================================================================

class Token(metaclass=ABCMeta):
    @abstractmethod
    def Parse(self, tokens, i):
        assert False


class TokenWhitespace(Token, metaclass=ABCMeta):
    def Parse(self, tokens, i):
        while i < len(tokens) and isinstance(tokens[i], TokenWhitespace):
            i += 1
        if i < len(tokens):
            return tokens[i].Parse(tokens, i)
        assert False

        return None


class TokenBinary(Token, metaclass=ABCMeta):
    def BinaryParse(self, tokens, i, node):
        i = parserEat(tokens, i, TokenParamL)
        node.left, i = tokens[i].Parse(tokens, i)
        i = parserEat(tokens, i, TokenComma)
        node.right, i = tokens[i].Parse(tokens, i)
        i = parserEat(tokens, i, TokenParamR)
        return node, i


class TokenUnparsable(Token, metaclass=ABCMeta):
    def Parse(self, tokens, i):
        print("Unable to parse.")
        assert False


# -------------------------------------------------------------------

class TokenPlus(TokenBinary):
    keyword = "+"

    def Parse(self, tokens, i):
        return super().BinaryParse(tokens, i + 1, AstAdd())


class TokenStar(TokenBinary):
    keyword = "*"

    def Parse(self, tokens, i):
        return super().BinaryParse(tokens, i + 1, AstMultiply())


class TokenIn(Token):
    keyword = "in"

    def Parse(self, tokens, i):
        i = parserEat(tokens, i + 1, TokenParamL)
        i = parserEat(tokens, i, TokenParamR)
        return AstIn(), i


class TokenOut(Token):
    keyword = "out"

    def Parse(self, tokens, i):
        node = AstOut()
        i = parserEat(tokens, i + 1, TokenParamL)
        node.child, i = tokens[i].Parse(tokens, i)
        i = parserEat(tokens, i, TokenParamR)
        return node, i


class TokenLess(TokenBinary):
    keyword = "<"

    def Parse(self, tokens, i):
        return super().BinaryParse(tokens, i + 1, AstLessThan())


class TokenEqual(TokenBinary):
    keyword = "="

    def Parse(self, tokens, i):
        return super().BinaryParse(tokens, i + 1, AstEqualTo())


class TokenIf(Token):
    keyword = "if"

    def Parse(self, tokens, i):
        node = AstIf()
        i = parserEat(tokens, i + 1, TokenParamL)
        node.cond, i = tokens[i].Parse(tokens, i)
        i = parserEat(tokens, i, TokenComma)
        node.iftrue, i = tokens[i].Parse(tokens, i)
        i = parserEat(tokens, i, TokenComma)
        node.iffalse, i = tokens[i].Parse(tokens, i)
        i = parserEat(tokens, i, TokenParamR)
        return node, i


class TokenParamL(TokenUnparsable):
    keyword = "("


class TokenParamR(TokenUnparsable):
    keyword = ")"


class TokenComma(TokenUnparsable):
    keyword = ","


class TokenSpace(TokenWhitespace):
    keyword = " "


class TokenNewline(TokenWhitespace):
    keyword = "\n"


class TokenLiteralInt(Token):
    def __init__(self, value):
        self.value = value

    def Parse(self, tokens, i):
        return AstIntLiteral(self.value), i + 1


# ===================================================================

class AstNode:
    pass


# -------------------------------------------------------------------

class AstAdd(AstNode):
    def __init__(self):
        self.left = None
        self.right = None

class AstMultiply(AstNode):
    def __init__(self):
        self.left = None
        self.right = None

class AstIn(AstNode):
    def __init__(self):
        pass

class AstOut(AstNode):
    def __init__(self):
        self.child = None

class AstLessThan(AstNode):
    def __init__(self):
        self.left = None
        self.right = None

class AstEqualTo(AstNode):
    def __init__(self):
        self.left = None
        self.right = None

class AstIf(AstNode):
    def __init__(self):
        self.cond = None
        self.iftrue = None
        self.iffalse = None

class AstIntLiteral(AstNode):
    def __init__(self, value):
        self.value = value


# ===================================================================

def read(path):
    with open(path, 'r') as f:
        return f.readline().strip()


def keyCompare(charstream, index, key):
    chomp = charstream[index:index + len(key)]
    return chomp == key


def literalCompare(charstream, i):
    j = i + 1
    while j < len(charstream) and charstream[j].isnumeric():
        j += 1
    return charstream[i:j]


def lexer(charstream):
    tokens = []
    i = 0
    while i < len(charstream):
        if keyCompare(charstream, i, TokenPlus.keyword):
            tokens.append(TokenPlus())
            i += len(TokenPlus.keyword)
        elif keyCompare(charstream, i, TokenStar.keyword):
            tokens.append(TokenStar())
            i += len(TokenStar.keyword)
        elif keyCompare(charstream, i, TokenIn.keyword):
            tokens.append(TokenIn())
            i += len(TokenIn.keyword)
        elif keyCompare(charstream, i, TokenOut.keyword):
            tokens.append(TokenOut())
            i += len(TokenOut.keyword)
        elif keyCompare(charstream, i, TokenLess.keyword):
            tokens.append(TokenLess())
            i += len(TokenLess.keyword)
        elif keyCompare(charstream, i, TokenEqual.keyword):
            tokens.append(TokenEqual())
            i += len(TokenEqual.keyword)
        elif keyCompare(charstream, i, TokenIf.keyword):
            tokens.append(TokenIf())
            i += len(TokenIf.keyword)
        elif keyCompare(charstream, i, TokenParamL.keyword):
            tokens.append(TokenParamL())
            i += len(TokenParamL.keyword)
        elif keyCompare(charstream, i, TokenParamR.keyword):
            tokens.append(TokenParamR())
            i += len(TokenParamR.keyword)
        elif keyCompare(charstream, i, TokenComma.keyword):
            tokens.append(TokenComma())
            i += len(TokenComma.keyword)
        elif keyCompare(charstream, i, TokenSpace.keyword):
            tokens.append(TokenSpace())
            i += len(TokenSpace.keyword)
        elif keyCompare(charstream, i, TokenNewline.keyword):
            tokens.append(TokenNewline())
            i += len(TokenNewline.keyword)
        else:
            isLit = literalCompare(charstream, i)
            if isLit:
                tokens.append(TokenLiteralInt(int(isLit)))
                i += len(isLit)
            else:
                raise False
    return tokens


def parserEat(tokens, i, tokenType):
    while isinstance(tokens[i], TokenWhitespace):
        i += 1

    if isinstance(tokens[i], tokenType):
        i += 1
    else:
        print("Unable to parse")
        assert False

    while i < len(tokens) and isinstance(tokens[i], TokenWhitespace):
        i += 1

    return i


def parser(tokens):
    ast, i = tokens[0].Parse(tokens, 0)
    assert i == len(tokens)
    return ast


def buildAst(path):
    chars = read(path)
    tokens = lexer(chars)
    ast = parser(tokens)
    return ast


if __name__ == '__main__':
    buildAst('ast.txt')
