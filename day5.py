
class OpCode:
    ADD = 1
    MULTIPLY = 2
    INPUT = 3
    OUTPUT = 4
    JMP_IF_TRUE = 5
    JMP_IF_FALSE = 6
    LESS = 7
    EQUAL = 8
    HALT = 99


class Mode:
    """enum style class"""
    PARAMETER = 0
    IMMEDIATE = 1


class Param:
    IN  = 0
    OUT = 1


def op_add(program, in1, in2, out):
    program[out] = in1 + in2

def op_multiply(program, in1, in2, out):
    program[out] = in1 * in2

def op_input(program, out):
    userInput = input("Input: ")
    program[out] = int(userInput)

def op_output(program, inp):
    print(": {}".format(inp))

def op_jumpIfTrue(program, in1, in2, curIp):
    if in1 != 0:
        return in2
    return curIp + 3

def op_jumpIfFalse(program, in1, in2, curIp):
    if in1 == 0:
        return in2
    return curIp + 3

def op_lessThan(program, in1, in2, out):
    program[out] = 1 if in1 < in2 else 0

def op_equalTo(program, in1, in2, out):
    program[out] = 1 if in1 == in2 else 0

def splitOpcode(code):
    if code < 0:
        return code, []
    op = code % 100
    code //= 100
    parammode = []
    for i in range(3):
        parammode.append(code % 10)
        code //= 10
    return op, parammode


def demodeInput(program, index, inOrOut, mode):
    if inOrOut == Param.IN:
        if mode == Mode.IMMEDIATE:
            return program[index]
        if mode == Mode.PARAMETER:
            return program[program[index]]
    elif inOrOut == Param.OUT:
        if mode == Mode.PARAMETER:
            return program[index]  # return the index
    assert(False)


def resolveArgs(program, index, listOfInOrOut, modes):
    return [demodeInput(program, index + i + 1, job, mode) for i, (job, mode) in enumerate(zip(listOfInOrOut, modes))]


def interpret(program):
    ip = 0  # instruction pointer
    while True:
        opcode, parammode = splitOpcode(program[ip])

        if opcode == OpCode.ADD:
            params = resolveArgs(program, ip, [Param.IN, Param.IN, Param.OUT], parammode)
            op_add(program, params[0], params[1], params[2])
            ip += 1 + len(params)
        elif opcode == OpCode.MULTIPLY:
            params = resolveArgs(program, ip, [Param.IN, Param.IN, Param.OUT], parammode)
            op_multiply(program, params[0], params[1], params[2])
            ip += 1 + len(params)
        elif opcode == OpCode.INPUT:
            params = resolveArgs(program, ip, [Param.OUT], parammode)
            op_input(program, params[0])
            ip += 1 + len(params)
        elif opcode == OpCode.OUTPUT:
            params = resolveArgs(program, ip, [Param.IN], parammode)
            op_output(program, params[0])
            ip += 1 + len(params)

        elif opcode == OpCode.JMP_IF_TRUE:
            params = resolveArgs(program, ip, [Param.IN, Param.IN], parammode)
            ip = op_jumpIfTrue(program, params[0], params[1], ip)

        elif opcode == OpCode.JMP_IF_FALSE:
            params = resolveArgs(program, ip, [Param.IN, Param.IN], parammode)
            ip = op_jumpIfFalse(program, params[0], params[1], ip)

        elif opcode == OpCode.LESS:
            params = resolveArgs(program, ip, [Param.IN, Param.IN, Param.OUT], parammode)
            op_lessThan(program, params[0], params[1], params[2])
            ip += 1 + len(params)
        elif opcode == OpCode.EQUAL:
            params = resolveArgs(program, ip, [Param.IN, Param.IN, Param.OUT], parammode)
            op_equalTo(program, params[0], params[1], params[2])
            ip += 1 + len(params)

        elif opcode == OpCode.HALT:
            return True

        else:
            return False


def day5a():
    with open('day5.txt', 'r') as f:
        program = [int(_) for _ in f.readline().split(',')]
    interpret(program)


if __name__ == '__main__':
    day5a()

# Part 1:
# input 1, expect a bunch of 0's, then 6745903
# Part 2:
# input 5, expect 9168267
