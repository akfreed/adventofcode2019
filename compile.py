import frontend
from day9 import Mode, OpCode
from abc import ABCMeta, abstractmethod


class Memory:
    pass

class MemoryLocation(Memory):
    def __init__(self):
        self.location = None
        self.mode = Mode.PARAMETER

    def Resolve(self, memBase, memory):
        if self.location is None:
            self.location = memBase + len(memory)
            memory.append(0)
        return self.location, self.mode


class MemoryConst(Memory):
    def __init__(self, value):
        self.value = value
        self.mode = Mode.IMMEDIATE

    def Resolve(self, memBase, memory):
        return self.value, self.mode


def pickOutputLocation(*args):
    # find an item in the list that is a location
    for arg in args:
        if arg.mode is Mode.PARAMETER:
            return arg
    # none found. Create new location
    return MemoryLocation()


def buildOpCode(opnum, mode1=0, mode2=0, mode3=0):
    return mode3 * 10000 + mode2 * 1000 + mode1 * 100 + opnum


class Code(metaclass=ABCMeta):
    def __init__(self, op):
        self.op = op

    @abstractmethod
    def Code(self, memBase, memory):
        pass

    @abstractmethod
    def Length(self):
        pass

class Binary(Code, metaclass=ABCMeta):
    def __init__(self, op):
        super().__init__(op)
        self.arg1 = None
        self.arg2 = None
        self.out = None

    def Code(self, memBase, memory):
        arg1, mode1 = self.arg1.Resolve(memBase, memory)
        arg2, mode2 = self.arg2.Resolve(memBase, memory)
        out, outMode = self.out.Resolve(memBase, memory)
        opCode = buildOpCode(self.op, mode1, mode2, outMode)
        return [opCode, arg1, arg2, out]

    def Length(self):
        return 4

class CodeAdd(Binary):
    def __init__(self):
        super().__init__(OpCode.ADD)

class CodeMultiply(Binary):
    def __init__(self):
        super().__init__(OpCode.MULTIPLY)

class CodeInput(Code):
    def __init__(self):
        super().__init__(OpCode.INPUT)
        self.out = None

    def Code(self, memBase, memory):
        out, outMode = self.out.Resolve(memBase, memory)
        opCode = buildOpCode(self.op, outMode)
        return [opCode, out]

    def Length(self):
        return 2

class CodeOutput(Code):
    def __init__(self):
        super().__init__(OpCode.OUTPUT)
        self.arg = None

    def Code(self, memBase, memory):
        arg, argMode = self.arg.Resolve(memBase, memory)
        opCode = buildOpCode(self.op, argMode)
        return [opCode, arg]

    def Length(self):
        return 2

class CodeJmpIfTrue(Code):
    def __init__(self):
        super().__init__(OpCode.JMP_IF_TRUE)
        self.cond = None
        self.loc = None

    def Code(self, memBase, memory):
        cond, condMode = self.cond.Resolve(memBase, memory)
        loc, locMode = self.loc.Resolve(memBase, memory)
        opCode = buildOpCode(self.op, condMode, locMode)
        return [opCode, cond, loc]

    def Length(self):
        return 3

class CodeJmpIfFalse(Code):
    def __init__(self):
        super().__init__(OpCode.JMP_IF_FALSE)
        self.cond = None
        self.loc = None

    def Code(self, memBase, memory):
        cond, condMode = self.cond.Resolve(memBase, memory)
        loc, locMode = self.loc.Resolve(memBase, memory)
        opCode = buildOpCode(self.op, condMode, locMode)
        return [opCode, cond, loc]

    def Length(self):
        return 3

class CodeLessThan(Binary):
    def __init__(self):
        super().__init__(OpCode.LESS)

class CodeEqualTo(Binary):
    def __init__(self):
        super().__init__(OpCode.EQUAL)

class CodeHalt(Code):
    def __init__(self):
        super().__init__(OpCode.HALT)

    def Code(self, memBase, memory):
        return [self.op]

    def Length(self):
        return 1



# instructions is a list of codes
# codes have variable length depending on number of arguments
def getInstructionsLength(instructions):
    length = 0
    for inst in instructions:
        length += inst.Length()
    return length


def generate(ast, instructions):
    if isinstance(ast, frontend.AstAdd):
        node = CodeAdd()
        node.arg1 = generate(ast.left, instructions)
        node.arg2 = generate(ast.right, instructions)
        node.out = pickOutputLocation(node.arg1, node.arg2)
        instructions.append(node)
        return node.out
    elif isinstance(ast, frontend.AstMultiply):
        node = CodeMultiply()
        node.arg1 = generate(ast.left, instructions)
        node.arg2 = generate(ast.right, instructions)
        node.out = pickOutputLocation(node.arg1, node.arg2)
        instructions.append(node)
        return node.out
    elif isinstance(ast, frontend.AstIn):
        node = CodeInput()
        node.out = MemoryLocation()
        instructions.append(node)
        return node.out
    elif isinstance(ast, frontend.AstOut):
        node = CodeOutput()
        node.arg = generate(ast.child, instructions)
        instructions.append(node)
        return node.arg
    elif isinstance(ast, frontend.AstLessThan):
        node = CodeLessThan()
        node.arg1 = generate(ast.left, instructions)
        node.arg2 = generate(ast.right, instructions)
        node.out = pickOutputLocation(node.arg1, node.arg2)
        instructions.append(node)
        return node.out
    elif isinstance(ast, frontend.AstEqualTo):
        node = CodeEqualTo()
        node.arg1 = generate(ast.left, instructions)
        node.arg2 = generate(ast.right, instructions)
        node.out = pickOutputLocation(node.arg1, node.arg2)
        instructions.append(node)
        return node.out
    elif isinstance(ast, frontend.AstIf):
        # this one is a little tricky
        # 1. generate code for evaluating the condition
        # 2. afterwards comes the code for conditional jump
        # 3. then comes the code for the true branch
        # 4. then we need to unconditionally jump to after the false branch
        # 5. then the code for the false branch
        #
        # * somehow the jump labels need to be tracked.
        # * also, the if statement should return a value. This value should be whatever is returned by the branch
        #   that ends up executing.

        # since we don't have assignment, use 0 + x to achieve assignment
        # we want both the true and false branches to dump their result in the same location
        out = MemoryLocation()  # create a common memory location
        trueThing = CodeAdd()
        trueThing.arg1 = MemoryConst(0)
        trueThing.out = out
        falseThing = CodeAdd()
        falseThing.arg1 = MemoryConst(0)
        falseThing.out = out

        node = CodeJmpIfFalse()
        node.cond = generate(ast.cond, instructions)
        # set jump target location later
        instructions.append(node)

        trueThing.arg2 = generate(ast.iftrue, instructions)
        instructions.append(trueThing)

        # Use a conditional jump where the condition is always true
        jump = CodeJmpIfTrue()
        jump.cond = MemoryConst(1)
        # set jump target location later
        instructions.append(jump)

        # we finally can set the first jump location
        node.loc = MemoryConst(getInstructionsLength(instructions))

        # Now comes the code for the false branch
        falseThing.arg2 = generate(ast.iffalse, instructions)
        instructions.append(falseThing)

        # we finally can set the second jump location
        jump.loc = MemoryConst(getInstructionsLength(instructions))

        return out

    elif isinstance(ast, frontend.AstIntLiteral):
        return MemoryConst(ast.value)
    else:
        assert False


def link(instructions):
    code = []
    memBase = getInstructionsLength(instructions)
    memory = []
    for inst in instructions:
        code += inst.Code(memBase, memory)
    # concat the memory
    code += memory
    return code


if __name__ == '__main__':
    ast = frontend.buildAst('ast.txt')
    instructions = []
    generate(ast, instructions)
    instructions.append(CodeHalt())
    code = link(instructions)
    print(code)
