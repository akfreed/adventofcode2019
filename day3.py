
def loadInput():
    with open('day3.txt', 'r') as f:
        wire1 = f.readline().split(',')
        wire2 = f.readline().split(',')
    return wire1, wire2


# return the set (as a list) of states the wire touches
def walkState(wire):
    states = set()
    curpos = [0, 0]
    for command in wire:
        direction = command[0]
        magnitude = int(command[1:])
        if direction is 'R':
            y = curpos[1]
            for x in range(curpos[0], curpos[0] + magnitude + 1):
                states.add((x, y))
            curpos[0] += magnitude
        elif direction is 'L':
            y = curpos[1]
            for x in range(curpos[0], curpos[0] - magnitude - 1, -1):
                states.add((x, y))
            curpos[0] -= magnitude
        elif direction is 'U':
            x = curpos[0]
            for y in range(curpos[1], curpos[1] + magnitude + 1):
                states.add((x, y))
            curpos[1] += magnitude
        elif direction is 'D':
            x = curpos[0]
            for y in range(curpos[1], curpos[1] - magnitude - 1, -1):
                states.add((x, y))
            curpos[1] -= magnitude
        else:
            raise
    return states


def findTarget(wire, target):
    curpos = [0, 0]
    numsteps = 0
    for command in wire:
        direction = command[0]
        magnitude = int(command[1:])

        if direction is 'R':
            for x in range(curpos[0], curpos[0] + magnitude):
                curpos[0] += 1
                numsteps += 1
                if curpos[0] == target[0] and curpos[1] == target[1]:
                    return numsteps
        elif direction is 'L':
            for x in range(curpos[0], curpos[0] - magnitude, -1):
                curpos[0] -= 1
                numsteps += 1
                if curpos[0] == target[0] and curpos[1] == target[1]:
                    return numsteps
        elif direction is 'U':
            for y in range(curpos[1], curpos[1] + magnitude):
                curpos[1] += 1
                numsteps += 1
                if curpos[0] == target[0] and curpos[1] == target[1]:
                    return numsteps
        elif direction is 'D':
            for y in range(curpos[1], curpos[1] - magnitude, -1):
                curpos[1] -= 1
                numsteps += 1
                if curpos[0] == target[0] and curpos[1] == target[1]:
                    return numsteps
        else:
            raise
    return None



def day3a():
    wire1, wire2 = loadInput()
    set1 = walkState(wire1)
    set2 = walkState(wire2)
    # find intersection of sets
    crosses = set1.intersection(set2)
    sorted = list(crosses)
    sorted.sort(key = lambda tup: abs(tup[0]) + abs(tup[1]))
    print(sorted)
    print(sorted[1])
    print(abs(sorted[1][0]) + abs(sorted[1][1]))


def day3b():
    wire1, wire2 = loadInput()
    set1 = walkState(wire1)
    set2 = walkState(wire2)
    # find intersection of sets
    crosses = set1.intersection(set2)
    crosses.remove((0, 0))

    m = min(findTarget(wire1, x) + findTarget(wire2, x) for x in crosses)
    print(m)


day3b()
