import frontend

def evaluate(ast):
    if isinstance(ast, frontend.AstAdd):
        return evaluate(ast.left) + evaluate(ast.right)
    elif isinstance(ast, frontend.AstMultiply):
        return evaluate(ast.left) * evaluate(ast.right)
    elif isinstance(ast, frontend.AstIn):
        return int(input("Input: "))
    elif isinstance(ast, frontend.AstOut):
        val = evaluate(ast.child)
        print(val)
        return val
    elif isinstance(ast, frontend.AstLessThan):
        return 1 if evaluate(ast.left) < evaluate(ast.right) else 0
    elif isinstance(ast, frontend.AstEqualTo):
        return 1 if evaluate(ast.left) == evaluate(ast.right) else 0
    elif isinstance(ast, frontend.AstIf):
        if evaluate(ast.cond) == 1:
            return evaluate(ast.iftrue)
        else:
            return evaluate(ast.iffalse)
    elif isinstance(ast, frontend.AstIntLiteral):
        return ast.value
    else:
        assert False


if __name__ == '__main__':
    ast = frontend.buildAst('ast.txt')
    evaluate(ast)
