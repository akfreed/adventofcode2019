from intcode import Process, loadProgram


def day23():
    program = loadProgram('day23.txt')
    procs = [Process(program, i) for i in range(50)]
    for proc in procs:
        proc.echoOutput = False
        proc.echoInput = False
        proc.PushInput(proc.pid)
        proc.PushInput(-1)

    xSave = None
    ySave = None
    ySet = set()

    first = True

    while True:
        idle = True
        for proc in procs:
            proc.Interpret()
            if proc.HasOutput():
                idle = False
                dest = proc.PopOutput()
                x = proc.PopOutput()
                y = proc.PopOutput()
                # print('dest: {}, x: {}, y: {}'.format(dest, x, y))
                if dest == 255:
                    if first:
                        print('first dest 255. x: {}, y: {}'.format(x, y))
                        first = False
                    xSave = x
                    ySave = y
                    continue

                procs[dest].PushInput(x)
                procs[dest].PushInput(y)

        if idle:
            procs[0].PushInput(xSave)
            procs[0].PushInput(ySave)
            if ySave in ySet:
                print('First duplicate. x: {}, y: {}'.format(xSave, ySave))
                break
            ySet.add(ySave)


if __name__ == '__main__':
    day23()
