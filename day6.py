from pathfinding import Dijkstra


def getInput():
    with open('day6.txt', 'r') as f:
        orbits = [_.strip().split(')') for _ in f.readlines()]
    return orbits


def getDegree(key, tree):
    if key == 'COM':
        return 0
    return 1 + getDegree(tree[key], tree)


def mapFromCom(key, tree, path):
    path = [key] + path
    if key == 'COM':
        return path
    return mapFromCom(tree[key], tree, path)


def day6a():
    orbits = getInput()
    tree = {}
    for pair in orbits:
        tree[pair[1]] = pair[0]

    count = 0
    for key in tree:
        count += getDegree(key, tree)

    print(count)


def day6b():
    orbits = getInput()
    tree = {}
    for pair in orbits:
        tree[pair[1]] = pair[0]

    pathYou = mapFromCom('YOU', tree, [])
    pathSan = mapFromCom('SAN', tree, [])
    # eliminate common
    x = 0
    for i, j in zip(pathYou, pathSan):
        if i == j:
            x += 1
        else:
            break

    pathYou = pathYou[x:-1]
    pathSan = pathSan[x:-1]

    print(pathYou)
    print(pathSan)

    print(len(pathYou) + len(pathSan))


def day6c():
    # redo the part 1 and part 2 solutions using pathfinding class
    orbits = getInput()
    tree = {}
    for pair in orbits:
        tree[pair[1]] = pair[0]

    d = Dijkstra()
    for key in tree:
        d.addNode(key, tree[key])
    d.addNode('COM', None)

    for key in tree:
        d.addEdge(key, tree[key], directed=False)

    d.calcDistances('COM')
    print(sum([orbit.distance for orbit in d.nodes.values()]))

    path = d.findPath('YOU', 'SAN')
    print(path)
    print(len(path) - 3)


day6c()
