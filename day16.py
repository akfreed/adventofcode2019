from math import ceil


def getInput():
    with open('day16.txt', 'r') as file:
        line = file.readline().strip()
    return [int(c) for c in line], int(line[:7])


def getFirstDigit(num):
    return abs(num) % 10


def getPattern(index, givenLength):
    base = [0, 1, 0, -1]
    pattern = []
    for i in range(givenLength + 1):
        idx = (i // (index + 1)) % len(base)
        pattern.append(base[idx])
    return pattern[1:]


def calcIndex(numsIn, index):
    pattern = getPattern(index, len(numsIn))
    o = sum([ix * px for ix, px in zip(numsIn, pattern)])
    d = getFirstDigit(o)
    return d


def day16a():
    numsIn, _ = getInput()
    numsOut = [None] * len(numsIn)

    for phase in range(100):
        for i in range(len(numsOut)):
            numsOut[i] = calcIndex(numsIn, i)
        numsIn = [_ for _ in numsOut]
        if phase % 10 == 0:
            print('{}%...'.format(phase), end='')
    print('100%')
    print(numsOut[:8])


def day16b():
    numsIn, skip = getInput()
    numsIn.reverse()
    length = len(numsIn) * 10000 - skip
    numsIn *= ceil(length / len(numsIn))
    numsIn = numsIn[:length]
    numsOut = [None] * length

    for phase in range(100):
        aggregator = 0
        for i in range(length):
            aggregator = (aggregator + numsIn[i]) % 10
            numsOut[i] = aggregator
        temp = numsIn
        numsIn = numsOut
        numsOut = temp
        if phase % 10 == 0:
            print('{}%...'.format(phase), end='')
    print('100%')

    answer = numsIn[-8:]
    answer.reverse()
    print(answer)


if __name__ == '__main__':
    day16a()
    day16b()
