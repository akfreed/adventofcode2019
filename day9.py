from intcode import Process, loadProgram


def day9a():
    program = loadProgram('day9.txt')

    p = Process(program)
    p.PushInput(1)
    p.Interpret()


def day9b():
    program = loadProgram('day9.txt')

    p = Process(program)
    p.PushInput(2)
    p.Interpret()


if __name__ == '__main__':
    day9a()
    day9b()
