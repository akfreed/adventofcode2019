from intcode import Process, loadProgram


def day13a():
    program = loadProgram('day13.txt')
    proc = Process(program)

    proc.Interpret()
    game = []
    while proc.HasOutput():
        x = proc.PopOutput()
        y = proc.PopOutput()
        t = proc.PopOutput()
        if t == 2:
            game.append((x, y, t))
    print(len(game))


def render(gameState):
    gameStateSorted = list(gameState.keys())
    gameStateSorted.sort(key=lambda t: (t[1], t[0]))

    prevy = None
    score = 0
    ostring = ""
    for pos in gameStateSorted:
        x, y = pos
        t = gameState[pos]

        if x == -1 and y == 0:
            score = t
            continue

        if prevy != y:
            ostring += '\n'
            prevy = y
        if t == 0:
            o = ' '
        elif t == 1:
            o = '+'
        elif t == 2:
            o = '#'
        elif t == 3:
            o = '='
        elif t == 4:
            o = 'o'
        else:
            assert False
        ostring += o

    ostring += "\n++++++++++++++++++++++++++++++++++++++++"
    ostring += "         score: {}".format(score)
    print(ostring)


def updateState(proc: Process, gameState, ball, paddle):
    while proc.HasOutput():
        x = proc.PopOutput()
        y = proc.PopOutput()
        t = proc.PopOutput()
        gameState[(x, y)] = t
        if t == 4:
            ball.x = x
            ball.y = y
        elif t == 3:
            paddle.x = x
            paddle.y = y


class Pos:
    def __init__(self, x, y):
        self.x = x
        self.y = y


def day13b():
    program = loadProgram('day13.txt')
    program[0] = 2
    proc = Process(program)
    proc.echoOutput = False
    proc.echoInput = False

    ball = Pos(0, 0)
    paddle = Pos(0, 0)
    gameState = {}

    while proc.Interpret() is None:
        updateState(proc, gameState, ball, paddle)
        render(gameState)  # remove to speed up
        if paddle.x < ball.x:
            proc.PushInput(1)
        elif paddle.x > ball.x:
            proc.PushInput(-1)
        else:
            proc.PushInput(0)

    updateState(proc, gameState, ball, paddle)
    render(gameState)


if __name__ == '__main__':
    day13b()
