
def getInput():
    with open('day22.txt', 'r') as file:
        return [line.strip() for line in file.readlines()]


class Shuffle:
    def act(self, deck):
        assert False

    def compose(self, m, b):
        assert False


class DealStack(Shuffle):
    def __init__(self):
        pass

    def act(self, deck):
        deck.reverse()
        return deck

    def compose(self, m, b):
        return -m, -b - 1


class Cut(Shuffle):
    def __init__(self, n):
        self.n = n

    def act(self, deck):
        return deck[self.n:] + deck[:self.n]

    def compose(self, m, b):
        return m, b - self.n


class DealIncrement(Shuffle):
    def __init__(self, increment):
        self.increment = increment

    def act(self, deck):
        newDeck = [0] * len(deck)
        for i in range(len(deck)):
            idx = (i * self.increment) % len(deck)
            newDeck[idx] = deck[i]
        return newDeck

    def compose(self, m, b):
        return self.increment * m, self.increment * b


def parseInput(lines):
    commands = []
    for line in lines:
        if line == 'deal into new stack':
            commands.append(DealStack())
        if line[:4] == 'cut ':
            commands.append(Cut(int(line[4:])))
        elif line[:20] == 'deal with increment ':
            commands.append(DealIncrement(int(line[20:])))
    return commands


def day22a():
    lines = getInput()
    commands = parseInput(lines)
    deck = [i for i in range(10007)]

    for command in commands:
        deck = command.act(deck)

    print(deck.index(2019))


def compose(commands, length):
    m = 1
    b = 0
    for command in commands:
        m, b = command.compose(m, b)
        m %= length
        b %= length
    return m, b


def show(commands, length):
    deck = [i for i in range(length)]

    for command in commands:
        deck = command.act(deck)

    print(deck)


def getBinary(num):
    binary = []
    while num > 0:
        binary.append(num % 2)
        num //= 2
    return binary


def selfCompose(m, b, binary, length):
    """Compose a function with itself N times where N is given in binary.
    The function is of the form y = mx + b
    Recursive function
    :param m: The coefficient of the function
    :param b: The constant of the function
    :param binary: A list of "bits" (1 or 0) with least-significant-bit (LSb) first
    :param length: The size of the deck (used for modulo)
    :return:
    """
    if not binary:
        return 1, 0

    newM, newB = selfCompose((m * m) % length, (m * b + b) % length, binary[1:], length)
    if binary[0]:
        newM = m * newM
        newB = m * newB + b
    return newM % length, newB % length


def congruence(a, b, m):
    """Solve a linear congruence--a function of the form ax === b mod m
    Here === means 'equivalent'
    Solve for x with a, b, and m given.
    Note that (ax + c) % m = b can be rearranged into this form.
    b mod m is a congruence class (all the numbers n | n % m == b)
    Iterative search scales linearly for big values of a, b, and m.
    from https://www.johndcook.com/blog/2008/12/10/solving-linear-congruences/
    This recursive solution works by solving the subproblem my === -b mod a, which is easier because a < m
        if y solves this problem, then x = (my + b)/a solves the original problem.
        my === 0 mod m
        my + b === b mod m
        a(my + b)/a === b mod m
        ax = b mod m where x === (my + b)/a
    a and m must be relatively prime for this method (e.g. gcd(a, m)==1)
        If b is not divisible by gcd(a, m), there are no solutions.
        If b is divisible by gcd(a, m), there are gcd(a, m) solutions.
        Thus if gcd(a, m)==1 there is exactly 1 solution.
    :param a: The coeficient of x. MUST be relatively prime with m. (e.g. gcd(a, m)==1)
    :param b: The resulted value after mod
    :param m: The mod. MUST be relatively prime with a.
    :return: x that solves ax % m == b
    """
    a %= m
    b %= m
    if m == 1:
        return 1
    y = congruence(m, -b, a)
    x = ((m * y + b) // a) % m
    return x


def day22b():
    lines = getInput()
    commands = parseInput(lines)
    length = 119315717514047
    # compose all the commands into one equation
    m, b = compose(commands, length)
    # compose mx + b with itself N times
    binary = getBinary(101741582076661)
    m, b = selfCompose(m, b, binary, length)
    print('m: {}'.format(m))
    print('b: {}'.format(b))
    # find out what ends up in slot 2020
    x = congruence(m, (2020 - b) % length, length)
    print('slot 2020: {}'.format(x))


if __name__ == '__main__':
    day22a()
    print()
    day22b()
