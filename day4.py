#def loadInput():
#    with open('day4.txt', 'r') as f:

rangeStart = 245182
rangeStop = 790572


def hasDouble(num):
    s = str(num)
    prev = ''
    for char in s:
        if char == prev:
            return True
        prev = char
    return False


def countNeighbors(l, idx):
    start = idx
    stop = idx

    while stop < len(l) and l[stop] == l[idx]:
        stop += 1

    while start >= 0 and l[start] == l[idx]:
        start -= 1
    start += 1

    return stop - start


def hasPair(num):
    s = str(num)
    prev = ''

    for i in range(len(s)):
        if s[i] == prev and countNeighbors(s, i) == 2:
            return True
        prev = s[i]
    return False



def onlyIncreases(num):
    s = str(num)
    prev = '-1'
    for char in s:
        if int(char) < int(prev):
            return False
        prev = char
    return True


def check(num):
    return hasDouble(num) and onlyIncreases(num)


def check2(num):
    return hasPair(num) and onlyIncreases(num)


def day4a():
    count = 0
    for i in range(rangeStart, rangeStop + 1):
        if check(i):
            count += 1

    print(count)



def day4b():
    count = 0
    for i in range(rangeStart, rangeStop + 1):
        if check2(i):
            count += 1

    print(count)


day4b()
