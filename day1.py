





def day1a():
    with open('day1.txt', 'r') as f:
        result = sum([int(line) // 3 - 2 for line in f.readlines()])
    print(result)


def getFuelSum(num):
    fuelCost = 0
    while num > 0:
        num = num // 3 - 2
        if num > 0:
            fuelCost += num
    return fuelCost


def day1b():
    with open('day1.txt', 'r') as f:
        values = [int(line) for line in f.readlines()]
    result = sum([getFuelSum(v) for v in values])
    print(result)


day1b()
