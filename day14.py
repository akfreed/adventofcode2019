import math


class Chem:
    def __init__(self, name, ingredients, batchAmount):
        self.name = name
        self.ingredients = ingredients
        self.batchAmount = batchAmount  # amount produced by 1 batch of production
        self.amountOnhand = 0

    def __repr__(self):
        return "{}: {}".format(self.name, self.amountOnhand)

    def reset(self):
        self.amountOnhand = 0

    def produce(self, amount):
        if self.amountOnhand >= amount:
            self.amountOnhand -= amount
            return True
        amount -= self.amountOnhand

        # figure out how many batches we need to make
        numBatches = math.ceil(amount / self.batchAmount)

        for numRequired, ingredient in self.ingredients:
            result = ingredient.produce(numRequired * numBatches)
            if result is False:
                return False

        self.amountOnhand = numBatches * self.batchAmount - amount
        return True


class Ore(Chem):
    def __init__(self):
        super().__init__('ORE', [], 1)
        self.count = 0
        self.limit = None

    def __repr__(self):
        return "{}: {}".format(self.name, self.count)

    def reset(self):
        super().reset()
        self.count = 0

    def produce(self, amount):
        if self.limit is not None and self.count + amount > self.limit:
            return False
        self.count += amount
        return True


class ProductionChain:
    def __init__(self, reactions):
        self.reactions = reactions

    def reset(self):
        for chem in self.reactions.values():
            chem.reset()

    def setOreLimit(self, amount):
        ore = self.reactions['ORE']
        ore.limit = amount

    def oreUsed(self):
        ore = self.reactions['ORE']
        return ore.count

    def produceFuel(self, amount):
        fuel = self.reactions['FUEL']
        return fuel.produce(amount)


def getInput():
    with open('day14.txt', 'r') as file:
        lines = [line.strip() for line in file.readlines()]

    names = {}
    for line in lines:
        productions, produced = line.split('=>')
        produced = produced.strip().split(' ')
        produced = (int(produced[0]), produced[1])

        productions = [p.strip().split(' ') for p in productions.split(',')]
        productions = [(int(num), name) for num, name in productions]

        names[produced[1]] = Chem(produced[1], productions, produced[0])
    names['ORE'] = Ore()

    # now that all are loaded, resolve the ingredients into actual links
    for name in names:
        chem = names[name]
        links = []
        for (needed, otherName) in chem.ingredients:
            links.append((needed, names[otherName]))
        chem.ingredients = links
    return names


def day14a():
    reactions = getInput()
    chain = ProductionChain(reactions)
    chain.produceFuel(1)
    print("Ore needed fore 1 fuel: {}".format(chain.oreUsed()))


def day14b():
    reactions = getInput()
    chain = ProductionChain(reactions)
    chain.setOreLimit(1000000000000)

    # find limit
    toProduce = 2 ** 10
    count = 0
    while True:
        if chain.produceFuel(toProduce) is False:
            break
        count += toProduce
        toProduce *= 2

    # binary search
    toProduce //= 2
    while toProduce > 0:
        chain.reset()
        chain.produceFuel(count)

        if chain.produceFuel(toProduce) is True:
            count += toProduce

        toProduce //= 2

    print("Fuel that can be produced with 1 trillion ore: {}".format(count))


if __name__ == '__main__':
    day14a()
    day14b()
