import itertools


# ===================================================================
class ProcBlock:
    def __init__(self, program, pid):
        self.program = [_ for _ in program]
        self.ip = 0
        self.pid = pid
        self.input = []
        self.output = []
        self.context = interpret(self)
        self.keyboardInputAllowed = False  # will request user input if the input queue is empty

    def PushInput(self, value):
        """The clientside function for sending client -> process communication."""
        self.input.append(value)

    def HasInput(self):
        return True if self.input or self.keyboardInputAllowed else False

    def PopInput(self):
        """The process-side function for receiving client -> process communication."""
        if not self.input:
            inp = int(input("[{}] <- Keyboard Input: ".format(self.pid)))
            self.PushInput(inp)
        return self.input.pop(0)

    def PushOutput(self, value):
        """The process-side function for process -> client communication."""
        self.output.append(value)

    def HasOutput(self):
        return True if self.output else False

    def PopOutput(self):
        """The clientside function for process -> client communication."""
        return self.output.pop(0)

    def Interpret(self):
        return next(self.context)


# ===================================================================

class OpCode:
    ADD = 1
    MULTIPLY = 2
    INPUT = 3
    OUTPUT = 4
    JMP_IF_TRUE = 5
    JMP_IF_FALSE = 6
    LESS = 7
    EQUAL = 8
    HALT = 99


class Mode:
    """enum style class"""
    PARAMETER = 0
    IMMEDIATE = 1


class Param:
    IN  = 0
    OUT = 1


# ===================================================================

def op_add(program, in1, in2, out):
    program[out] = in1 + in2


def op_multiply(program, in1, in2, out):
    program[out] = in1 * in2


def op_input(program, out, proc: ProcBlock):
    inputVal = proc.PopInput()
    print("[{}] <- : {}".format(proc.pid, inputVal))
    program[out] = inputVal


def op_output(inp, proc: ProcBlock):
    proc.PushOutput(inp)
    print("[{}] -> : {}".format(proc.pid, inp))


def op_jumpIfTrue(in1, in2, curIp):
    if in1 != 0:
        return in2
    return curIp + 3


def op_jumpIfFalse(in1, in2, curIp):
    if in1 == 0:
        return in2
    return curIp + 3


def op_lessThan(program, in1, in2, out):
    program[out] = 1 if in1 < in2 else 0


def op_equalTo(program, in1, in2, out):
    program[out] = 1 if in1 == in2 else 0


# ===================================================================

def splitOpcode(code):
    if code < 0:
        return code, []
    op = code % 100
    code //= 100
    parammode = []
    for i in range(3):
        parammode.append(code % 10)
        code //= 10
    return op, parammode


def demodeInput(program, index, inOrOut, mode):
    if inOrOut == Param.IN:
        if mode == Mode.IMMEDIATE:
            return program[index]
        if mode == Mode.PARAMETER:
            return program[program[index]]
    elif inOrOut == Param.OUT:
        if mode == Mode.PARAMETER:
            return program[index]  # return the index
    assert False


def resolveArgs(program, index, listOfInOrOut, modes):
    return [demodeInput(program, index + i + 1, job, mode) for i, (job, mode) in enumerate(zip(listOfInOrOut, modes))]


# ===================================================================

def interpret(proc: ProcBlock):
    program = proc.program
    ip = proc.ip  # instruction pointer

    if ip >= len(program):
        yield False

    while True:
        opcode, parammode = splitOpcode(program[ip])

        if opcode == OpCode.ADD:
            params = resolveArgs(program, ip, [Param.IN, Param.IN, Param.OUT], parammode)
            op_add(program, params[0], params[1], params[2])
            ip += 1 + len(params)
        elif opcode == OpCode.MULTIPLY:
            params = resolveArgs(program, ip, [Param.IN, Param.IN, Param.OUT], parammode)
            op_multiply(program, params[0], params[1], params[2])
            ip += 1 + len(params)

        elif opcode == OpCode.INPUT:
            params = resolveArgs(program, ip, [Param.OUT], parammode)

            while proc.HasInput() is False:
                proc.ip = ip
                yield None

            op_input(program, params[0], proc)
            ip += 1 + len(params)

        elif opcode == OpCode.OUTPUT:
            params = resolveArgs(program, ip, [Param.IN], parammode)
            op_output(params[0], proc)
            ip += 1 + len(params)

        elif opcode == OpCode.JMP_IF_TRUE:
            params = resolveArgs(program, ip, [Param.IN, Param.IN], parammode)
            ip = op_jumpIfTrue(params[0], params[1], ip)

        elif opcode == OpCode.JMP_IF_FALSE:
            params = resolveArgs(program, ip, [Param.IN, Param.IN], parammode)
            ip = op_jumpIfFalse(params[0], params[1], ip)

        elif opcode == OpCode.LESS:
            params = resolveArgs(program, ip, [Param.IN, Param.IN, Param.OUT], parammode)
            op_lessThan(program, params[0], params[1], params[2])
            ip += 1 + len(params)
        elif opcode == OpCode.EQUAL:
            params = resolveArgs(program, ip, [Param.IN, Param.IN, Param.OUT], parammode)
            op_equalTo(program, params[0], params[1], params[2])
            ip += 1 + len(params)

        elif opcode == OpCode.HALT:
            yield True

        else:
            yield False


# ===================================================================

def getInput():
    with open('day7.txt', 'r') as f:
        program = [int(_) for _ in f.readline().split(',')]
    return program


# ===================================================================

def day7a():
    program = getInput()

    maxAmpl = -1
    maxPerm = []
    for perm in itertools.permutations([0, 1, 2, 3, 4]):

        procs = [ProcBlock(program, p) for p in perm]
        for proc, p in zip(procs, perm):
            proc.PushInput(p)
        procs[0].PushInput(0)

        for i in range(5):
            procs[i].Interpret()
            procs[(i + 1) % 5].PushInput(procs[i].PopOutput())

        ampl = procs[0].PopInput()
        if ampl > maxAmpl:
            maxAmpl = ampl
            maxPerm = [_ for _ in perm]

    print()
    print(maxAmpl)
    print(maxPerm)


def day7b():
    program = getInput()

    maxAmpl = -1
    maxPerm = []

    for perm in itertools.permutations([5, 6, 7, 8, 9]):
        procs = [ProcBlock(program, idx) for idx in perm]
        for proc, p in zip(procs, perm):
            proc.PushInput(p)
        procs[0].PushInput(0)

        res = None
        while res is None:
            for i in range(5):
                res = procs[i].Interpret()
                procs[(i + 1) % 5].PushInput(procs[i].PopOutput())

        assert(res is True)

        ampl = procs[0].PopInput()
        if ampl > maxAmpl:
            maxAmpl = ampl
            maxPerm = [_ for _ in perm]

    print()
    print(maxAmpl)
    print(maxPerm)


if __name__ == '__main__':
    day7b()
