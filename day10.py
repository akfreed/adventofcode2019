import math


def getInput():
    with open('day10.txt', 'r') as f:
        asteroidMap = [line.strip() for line in f.readlines()]

    asteroidPos = []
    for yAsteroid in range(len(asteroidMap)):
        for xAsteroid in range(len(asteroidMap[0])):
            if asteroidMap[yAsteroid][xAsteroid] == '.':
                continue  # not an asteroid
            asteroidPos.append((xAsteroid, yAsteroid))
    return asteroidPos


class Asteroid:
    def __init__(self, x, y, xOrig, yOrig):
        self.x = x
        self.y = y
        self.dx = x - xOrig
        self.dy = yOrig - y
        self.dist = math.sqrt(self.dx**2 + self.dy**2)
        # starts pointing up...switch x and y for atan calculation
        self.theta = math.atan2(self.dx, self.dy)
        if self.theta < 0:
            self.theta += 2 * math.pi
        self.queuePosition = 0


def floatApproxEquals(f1, f2, tolerance=0.00001):
    return True if abs(f1 - f2) < tolerance else False


def buildAsteroidInfo(asteroidPositions, stationPosition):
    xStation, yStation = stationPosition
    asteroidInfos = []

    for pos in asteroidPositions:
        xAsteroid, yAsteroid = pos
        if pos == stationPosition:
            continue
        asteroid = Asteroid(xAsteroid, yAsteroid, xStation, yStation)
        asteroidInfos.append(asteroid)

    # sort asteroidInfos by theta
    asteroidInfos.sort(key=lambda a: a.theta)

    # combine answers that are close
    for i in range(len(asteroidInfos)):
        if i + 1 < len(asteroidInfos) and floatApproxEquals(asteroidInfos[i].theta, asteroidInfos[i + 1].theta):
            asteroidInfos[i].theta = asteroidInfos[i + 1].theta

    # sort by theta and distance
    asteroidInfos.sort(key=lambda a: (a.theta, a.dist))

    # set the queue position
    for i in range(len(asteroidInfos)):
        if i + 1 < len(asteroidInfos) and asteroidInfos[i].theta == asteroidInfos[i + 1].theta:
            asteroidInfos[i + 1].queuePosition = asteroidInfos[i].queuePosition + 1

    return asteroidInfos


def day10a():
    asteroidPositions = getInput()

    maxVisible = -1
    maxIdx = 0
    maxInfos = None

    # for each base position
    for stationPos in asteroidPositions:
        asteroidInfos = buildAsteroidInfo(asteroidPositions, stationPos)

        numVisible = [info.queuePosition for info in asteroidInfos].count(0)

        if numVisible > maxVisible:
            maxVisible = numVisible
            maxIdx = stationPos
            maxInfos = asteroidInfos

    print("x: {}, y: {}".format(maxIdx[0], maxIdx[1]))
    print("Max Visible: {}".format(maxVisible))

    return maxInfos


def day10b():
    asteroidInfos = day10a()

    # now we can sort by queue position and theta
    asteroidInfos.sort(key=lambda a: (a.queuePosition, a.theta))

    print("200th asteroid: ({}, {})".format(asteroidInfos[199].x, asteroidInfos[199].y))


if __name__ == '__main__':
    day10b()
