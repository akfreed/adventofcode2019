from intcode import Process, loadProgram
from pathfinding import Dijkstra
from utility import Vector, render_map


def explore(proc: Process, tileMap, position: Vector):
    """At the end of the exploration (recursive graph traversal) the tileMap will be filled out."""
    
    # explore north neighbor
    neighbor = position.up()
    if neighbor not in tileMap:
        # tell the program to go north (input 1)
        proc.PushInput(1)
        # resume the program
        proc.Interpret()
        # get the output
        tileType = proc.PopOutput()
        tileMap[neighbor] = tileType
        # if output is not 0, move was successful (otherwise, it was a wall and the robot didn't move)
        if tileType != 0:
            # recursive exploration
            explore(proc, tileMap, neighbor)
            # now that we're back from recursive exploration, return the 
            # robot to the position at the beginning of this function.
            # tell the program to go south (input 2)
            proc.PushInput(2)
            # run
            proc.Interpret()
            # ignore output
            proc.PopOutput()

    # explore south neighbor
    neighbor = position.down()
    if neighbor not in tileMap:
        proc.PushInput(2)
        proc.Interpret()
        tileType = proc.PopOutput()
        tileMap[neighbor] = tileType
        if tileType != 0:
            explore(proc, tileMap, neighbor)
            proc.PushInput(1)
            proc.Interpret()
            proc.PopOutput()

    # explore west neighbor
    neighbor = position.left()
    if neighbor not in tileMap:
        proc.PushInput(3)
        proc.Interpret()
        tileType = proc.PopOutput()
        tileMap[neighbor] = tileType
        if tileType != 0:
            explore(proc, tileMap, neighbor)
            proc.PushInput(4)
            proc.Interpret()
            proc.PopOutput()

    # explore east neighbor
    neighbor = position.right()
    if neighbor not in tileMap:
        proc.PushInput(4)
        proc.Interpret()
        tileType = proc.PopOutput()
        tileMap[neighbor] = tileType
        if tileType != 0:
            explore(proc, tileMap, neighbor)
            proc.PushInput(3)
            proc.Interpret()
            proc.PopOutput()


def day15a():
    program = loadProgram('day15.txt')
    proc = Process(program)
    proc.echoInput = False
    proc.echoOutput = False

    origin = Vector(0, 0)
    tileMap = {origin: 1}
    explore(proc, tileMap, origin)

    renderMapping = {
        0: '##',
        1: '  ',
        2: 'O ',
    }
    render_map(tileMap, renderMapping, 0, 'X ')

    oxygenSystem = None
    for key in tileMap:
        if tileMap[key] == 2:
            oxygenSystem = key
            break
    print('Position of Oxygen Sensor: {}'.format(oxygenSystem))

    pathfinder = Dijkstra()

    # add the tiles to the pathfinder (without the walls)
    for key in tileMap:
        if tileMap[key] != 0:
            pathfinder.addNode(key, tileMap[key])

    # link nodes manhattan style
    pathfinder.autoLinkManhattan()
    path = pathfinder.findPath(origin, oxygenSystem)
    print('Length of path: {}'.format(len(path) - 1))


def day15b():
    program = loadProgram('day15.txt')
    proc = Process(program)
    proc.echoInput = False
    proc.echoOutput = False

    origin = Vector(0, 0)
    tileMap = {origin: 1}
    explore(proc, tileMap, origin)

    renderMapping = {
        0: '##',
        1: '  ',
        2: 'O ',
    }
    render_map(tileMap, renderMapping, 0, 'X ')

    oxygenSystem = None
    for key in tileMap:
        if tileMap[key] == 2:
            oxygenSystem = key
            break
    print('Position of Oxygen Sensor: {}'.format(oxygenSystem))

    pathfinder = Dijkstra()

    # add the tiles to the pathfinder (without the walls)
    for key in tileMap:
        if tileMap[key] != 0:
            pathfinder.addNode(key, tileMap[key])

    # link nodes manhattan style
    pathfinder.autoLinkManhattan()
    pathfinder.calcDistances(oxygenSystem)

    maxDistance = max([node.distance for node in pathfinder.nodes.values()])
    print('Max distance from Oxygen System: {}'.format(maxDistance))


if __name__ == '__main__':
    day15a()
    day15b()
