from intcode import OpCode, Mode, splitOpcode


def loadProgram(path):
    with open(path, 'r') as f:
        return [int(_) for _ in f.readline().split(',')]


def decompile(program):
    ip = 0
    while ip < len(program):
        opcode, parammodes = splitOpcode(program[ip])

        validInstruction = True
        instruction = ""
        numParams = 0

        if opcode == OpCode.ADD:
            instruction = "ADD"
            numParams = 3
        elif opcode == OpCode.MULTIPLY:
            instruction = "MULT"
            numParams = 3
        elif opcode == OpCode.INPUT:
            instruction = "IN"
            numParams = 1
        elif opcode == OpCode.OUTPUT:
            instruction = "OUT"
            numParams = 1
        elif opcode == OpCode.JMP_IF_TRUE:
            instruction = "JNZ"
            numParams = 2
        elif opcode == OpCode.JMP_IF_FALSE:
            instruction = "JZ"
            numParams = 2
        elif opcode == OpCode.LESS:
            instruction = "LESS"
            numParams = 3
        elif opcode == OpCode.EQUAL:
            instruction = "EQUAL"
            numParams = 3
        elif opcode == OpCode.REL_BASE:
            instruction = "REBASE"
            numParams = 1
        elif opcode == OpCode.HALT:
            instruction = "HALT"
            numParams = 0
        else:
            validInstruction = False

        # if one of the modes is not an available mode, it's likely that the given opcode is wrong
        if any([mode not in (Mode.PARAMETER, Mode.IMMEDIATE, Mode.RELATIVE) for mode in parammodes]):
            validInstruction = False

        # an instruction must have room left in the program for its parameters
        if ip + numParams >= len(program):
            validInstruction = False

        if not validInstruction:
            # some codes look like they might be be instructions
            if not instruction:
                instruction = "_{}_".format(opcode)
            else:
                instruction = "_{}_ ({}?)".format(opcode, instruction)
            numParams = 0


        print("{}: {} ({})".format(ip, instruction, program[ip]))
        for i in range(numParams):
            if parammodes[i] is Mode.PARAMETER:
                rep = ('[', ']')
            elif parammodes[i] is Mode.IMMEDIATE:
                rep = ('', '')
            elif parammodes[i] is Mode.RELATIVE:
                rep = ('<', '>')
            else:
                assert(False)
            print("{0}:     {2}{1}{3}".format(ip + i + 1, program[ip + i + 1], rep[0], rep[1]))

        ip += numParams + 1


def main():
    program = loadProgram('decomp.txt')
    decompile(program)


if __name__ == '__main__':
    main()
