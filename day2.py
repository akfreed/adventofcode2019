def interpret(program):
    ip = 0  # instruction pointer
    while True:
        opcode = program[ip]
        if opcode == 1 or opcode == 2:
            input1Pos = program[ip + 1]
            input2Pos = program[ip + 2]
            outputPos = program[ip + 3]
            input1 = program[input1Pos]
            input2 = program[input2Pos]
            if opcode == 1:
                output = input1 + input2
            else:
                output = input1 * input2
            program[outputPos] = output
            ip += 4
        elif opcode == 99:
            return True
        else:
            return False


def day2a():
    with open('day2.txt', 'r') as f:
        program = [int(num) for num in f.readline().split(',')]

    # "restore program state"
    program[1] = 12
    program[2] = 2

    interpret(program)
    print(program[0])


def day2b():
    with open('day2.txt', 'r') as f:
        program = [int(num) for num in f.readline().split(',')]

    for noun in range(100):
        for verb in range(100):
            copy = [_ for _ in program]
            copy[1] = noun
            copy[2] = verb
            success = interpret(copy)
            if success:
                output = copy[0]
                if output == 19690720:
                    print("Found good inputs noun={} and verb={}".format(noun, verb))








day2b()
