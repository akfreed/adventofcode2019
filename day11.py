from intcode import Process, loadProgram
from utility import Vector, render_map


def runRobot(proc: Process):
    pos = Vector(0, 0)
    color = {}
    direction = 0

    while proc.Interpret() is None:
        outColor = proc.PopOutput()
        outTurn = proc.PopOutput()

        color[pos] = outColor

        if outTurn == 0:
            direction = (direction + 1) % 4
        else:
            direction = (direction - 1 + 4) % 4

        if direction == 0:
            pos = pos.up()
        elif direction == 1:
            pos = pos.left()
        elif direction == 2:
            pos = pos.down()
        elif direction == 3:
            pos = pos.right()
        else:
            assert False

        if pos in color:
            inColor = color[pos]
        else:
            inColor = 0

        proc.PushInput(inColor)

    return color


def day11a():
    program = loadProgram('day11.txt')
    proc = Process(program)
    proc.echoInput = False
    proc.echoOutput = False
    proc.PushInput(0)
    color = runRobot(proc)
    print(len(color.keys()))


def day11b():
    program = loadProgram('day11.txt')
    proc = Process(program)
    proc.echoInput = False
    proc.echoOutput = False
    proc.PushInput(1)
    color = runRobot(proc)

    colorMap = {
        0: '  ',
        1: '##'
    }
    render_map(color, colorMap, 0)


if __name__ == '__main__':
    day11a()
    day11b()
