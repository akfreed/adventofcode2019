def getInput():
    with open('day8.txt', 'r') as f:
        return f.readline().strip()


def day8a():
    inp = getInput()
    layers = []
    z = 25 * 6
    print(len(inp))
    assert len(inp) % (z) == 0
    for i in range(len(inp) // (z)):
        layers.append(inp[(i * z):((i+1)* z)])

    minZeros = 1000000000
    minIdx = 0
    for i in range(len(layers)):
        numZeros = 0
        for num in layers[i]:
            if num == '0':
                numZeros += 1
        if numZeros < minZeros:
            minZeros = numZeros
            minIdx = i

    numOnes = 0
    numTwos = 0
    for num in layers[minIdx]:
        if num == '1':
            numOnes += 1
        elif num == '2':
            numTwos += 1
    print(numOnes * numTwos)




def day8b():
    inp = getInput()
    layers = []
    z = 25 * 6
    print(len(inp))
    assert len(inp) % (z) == 0
    for i in range(len(inp) // (z)):
        layers.append(inp[(i * z):((i+1)* z)])

    xLayer = ['2' for _ in layers[0]]
    for layer in layers:
        for i in range(25 * 6):
            if xLayer[i] == '2':
                xLayer[i] = layer[i]

    for y in range(6):
        for x in range(25):
            val = xLayer[y * 25 + x]
            print("{}".format(' ' if val is '0' else 'O'), end="")
        print()


if __name__ == '__main__':
    day8b()
