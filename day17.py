from intcode import Process, loadProgram
from utility import Vector, Turtle
from pathfinding import Dijkstra


def day17a():
    program = loadProgram('day17.txt')
    p = Process(program, additionalMemory=3000)
    p.echoOutput = False
    p.Interpret()

    stream = [c for c in p.output]
    while p.HasOutput():
        c = p.PopOutput()
        print(chr(c), end='')
        if c != 10:
            print(chr(c), end='')

    pos = Vector(0, 0)
    d = Dijkstra()
    for c in stream:
        if c == 35:
            d.addNode(pos)
        elif c == 10:
            pos = pos.down()
            pos.x = 0
            continue
        pos = pos.right()

    d.autoLinkManhattan()

    intersections = []
    for node in d.nodes.values():
        if len(node.edges) == 4:
            intersections.append(node.identifier)

    print(sum([x * y for (x, y) in intersections]))


def str2ascii(str):
    return [ord(c) for c in str]


def getMap(proc: Process):
    proc.Interpret()

    nodes = set()
    pos = Vector(0, 0)
    origin = None
    while proc.HasOutput():
        c = proc.PopOutput()

        if c == ord('#'):
            nodes.add(pos.clone())
        elif c == ord('^'):
            nodes.add(pos.clone())
            origin = pos
        elif c == ord('\n'):
            pos = pos.down()
            pos.x = 0
            print(chr(c), end='')  # print once
            continue  # don't move right
        elif c == ord('.'):
            pass
        else:
            assert False

        print(chr(c), end='')  # print twice
        print(chr(c), end='')

        pos = pos.right()

    return nodes, origin


def getPath(nodes, origin):
    LEFT  = 'L'
    RIGHT = 'R'

    turtle = Turtle()
    turtle.pos = origin.clone()
    path = []
    while True:
        # rotate left or right
        if turtle.toLeft(1) in nodes:
            path.append(LEFT)
            turtle.turnLeft90()
        elif turtle.toRight(1) in nodes:
            path.append(RIGHT)
            turtle.turnRight90()
        else:
            break

        # move forward as far as possible
        count = 0
        while turtle.toFront(1) in nodes:
            turtle.forward(1)
            count += 1
        assert count > 0
        path.append(count)
    return path


def findPattern(stringList, depth, maxDepth=3):
    # filter the empties
    stringList = [s for s in stringList if s]
    if not stringList:
        return []
    if depth >= maxDepth:
        return False
    # as a slight optimization, pick the shortest string
    myString = min(stringList, key=lambda s: len(s))
    # test different substrings
    for i in range(1, min(20, len(myString)) + 1):
        substr = myString[:i]
        # split all the strings using this substring
        newList = []
        for s in stringList:
            newList += s.split(substr)
        # recursive call with this list
        ret = findPattern(newList, depth + 1, maxDepth)
        if ret is not False:
            return [substr] + ret
    return False


def day17b():
    program = loadProgram('day17.txt')
    process = Process(program, additionalMemory=3000)
    process.echoOutput = False

    # get the scaffolding positions
    nodes, origin = getMap(process)
    # get the path to the end
    path = getPath(nodes, origin)
    # find patterns
    path = ''.join([str(e) for e in path])
    patterns = findPattern([path], 0)

    a, b, c = patterns

    master = path.replace(a, 'A')
    master = master.replace(b, 'B')
    master = master.replace(c, 'C')

    # put the commas in
    a = a.replace('R', ',R,').replace('L', ',L,')[1:] + '\n'
    b = b.replace('R', ',R,').replace('L', ',L,')[1:] + '\n'
    c = c.replace('R', ',R,').replace('L', ',L,')[1:] + '\n'
    master = ','.join(master) + '\n'

    # reset the program / setup for part 2
    program[0] = 2
    process = Process(program, additionalMemory=3000)
    process.echoOutput = False
    process.echoInput = False

    # ascii encode
    mvt  = str2ascii(master)
    mvtA = str2ascii(a)
    mvtB = str2ascii(b)
    mvtC = str2ascii(c)
    video = str2ascii('n\n')

    # input
    for e in mvt + mvtA + mvtB + mvtC + video:
        process.PushInput(e)

    # run
    process.Interpret()
    print(process.output[-1])


if __name__ == '__main__':
    day17a()
    day17b()
