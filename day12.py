import math
import functools


def lcm(*args):
    """Return the LCM of multiple numbers.
    The numpy version of LCM is susceptible to overflow.
    There must be at least 2 arguments.
    """
    assert len(args) >= 2
    return functools.reduce(lambda a, b: a * b // math.gcd(a, b), args)


class Moon:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
        self.dx = 0
        self.dy = 0
        self.dz = 0

    def applyGravity(self, moons):
        for moon in moons:
            if moon == self:
                continue
            if moon.x > self.x:
                self.dx += 1
            elif moon.x < self.x:
                self.dx -= 1

            if moon.y > self.y:
                self.dy += 1
            elif moon.y < self.y:
                self.dy -= 1

            if moon.z > self.z:
                self.dz += 1
            elif moon.z < self.z:
                self.dz -= 1

    def applyVelocity(self):
        self.x += self.dx
        self.y += self.dy
        self.z += self.dz

    def totalEnergy(self):
        pot = abs(self.x)  + abs(self.y)  + abs(self.z)
        kin = abs(self.dx) + abs(self.dy) + abs(self.dz)
        return pot * kin


def getInput():
    moons = [
        Moon(-1, -4, 0),
        Moon(4, 7, -1),
        Moon(-14, -10, 9),
        Moon(1, 2, 17)]
    return moons


def day12a():
    moons = getInput()
    for i in range(1000):
        for moon in moons:
            moon.applyGravity(moons)
        for moon in moons:
            moon.applyVelocity()

    systemEnergy = 0
    for moon in moons:
        systemEnergy += moon.totalEnergy()
    print("Total system energy: {}".format(systemEnergy))


def day12b():
    moons = getInput()

    # simulate each dimension independently
    # each one has a period
    # the LCM of the periods is the answer
    vStates = [set(), set(), set()]

    # these lambdas define the state of the system for one dimension only
    getState = [lambda ms: tuple([(m.x, m.dx) for m in ms]),
                lambda ms: tuple([(m.y, m.dy) for m in ms]),
                lambda ms: tuple([(m.z, m.dz) for m in ms])]

    vPeriods = [None, None, None]

    print("Calculating...")

    count = 0
    done = False
    while not done:
        for moon in moons:
            moon.applyGravity(moons)
        for moon in moons:
            moon.applyVelocity()

        # for each dimension
        for i in range(3):
            # haven't found the period yet
            if vPeriods[i] is None:
                state = getState[i](moons)
                if state in vStates[i]:
                    vPeriods[i] = count
                    if all(vPeriods):
                        done = True
                else:
                    vStates[i].add(state)
        count += 1

    print("x period: {}".format(vPeriods[0]))
    print("y period: {}".format(vPeriods[1]))
    print("z period: {}".format(vPeriods[2]))

    print("LCM: {}".format(lcm(*vPeriods)))


if __name__ == '__main__':
    day12a()
    print()
    day12b()
