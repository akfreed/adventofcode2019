from intcode import Process, loadProgram


def getInput():
    proc = Process(loadProgram('day21.txt'))
    proc.echoInput = False
    proc.echoOutput = False
    return proc


def printOutput(p: Process):
    try:
        while p.HasOutput():
            c = p.PopOutput()
            print(chr(c), end='')
    except:
        print(c)


def str2ascii(str):
    return [ord(c) for c in str]


def addInput(process, str):
    for c in str2ascii(str):
        process.PushInput(c)


def day21a():
    process = getInput()

    commands = [
        'NOT A J\n',
        'NOT B T\n',
        'OR  T J\n',
        'NOT C T\n',
        'OR  T J\n',
        'AND D J\n',
        'WALK\n'
    ]
    for command in commands:
        addInput(process, command)

    process.Interpret()
    printOutput(process)


def day21b():
    print('Calculating...')
    process = getInput()

    commands = [
        'NOT A J\n',
        'AND A J\n',
        'OR  A J\n',
        'AND B J\n',
        'AND C J\n',
        'NOT J J\n',
        'AND D J\n',

        'NOT A T\n',
        'AND A T\n',
        'OR  E T\n',
        'OR  H T\n',
        'AND T J\n'

        'RUN\n'
    ]
    for command in commands:
        addInput(process, command)

    process.Interpret()
    printOutput(process)


if __name__ == '__main__':
    day21a()
    day21b()
