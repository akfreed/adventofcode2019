from intcode import Process, loadProgram
import itertools


N = 'north\n'
E = 'east\n'
W = 'west\n'
S = 'south\n'
T = 'take '
D = 'drop '

badItems = set()
badItems.add('giant electromagnet\n')
badItems.add('molten lava\n')
badItems.add('infinite loop\n')
badItems.add('photons\n')
badItems.add('escape pod\n')


def str2ascii(str):
    return [ord(c) for c in str]


def addInput(process, str):
    for c in str2ascii(str):
        process.PushInput(c)


def printOutput(p: Process):
    s = ''
    while p.HasOutput():
        c = p.PopOutput()
        s += chr(c)
        print(chr(c), end='')
    return s


def gatherAll():
    return E + T + 'weather machine\n' + W + W + W + T + 'bowl of rice\n' + E + N + T + 'polygon\n' + E + T + 'hypercube\n' + S + \
           T + 'dark matter\n' + N + W + N + T + 'candy cane\n' + W + N + T + 'manifold\n' + S + W + N + T + 'dehydrated water\n' + W


def testCheckpoint(process):
    addInput(process, S)
    process.Interpret()
    output = printOutput(process)
    if output.lower().find('alert') >= 0:
        return False
    return True


def take(items):
    s = ''
    for i in items:
        s += T + i
    return s


def drop(items):
    s = ''
    for i in items:
        s += D + i
    return s


def tryall(process, items):
    for i in range(len(items)):
        for combi in itertools.combinations(items, i):
            addInput(process, take(combi))
            res = testCheckpoint(process)
            if res:
                return
            addInput(process, drop(combi))


def explore():
    program = loadProgram('day25.txt')
    process = Process(program)
    process.echoOutput = False
    process.echoInput = False

    res = process.Interpret()

    while res is None:
        printOutput(process)
        inp = input() + '\n'
        addInput(process, inp)
        res = process.Interpret()

    printOutput(process)


def parseMessage(message):
    roomName = message.strip().split('\n')[0].split('==')[1].strip()
    idxDirections = message.find('Doors here lead:\n')
    idxItems = message.find('Items here:\n')
    items = []
    if idxItems != -1:
        items = [line[2:].strip() + '\n' for line in message[idxItems:].split('\n') if line[:2] == '- ']
    else:
        idxItems = len(message)

    assert idxDirections != -1
    directions = [line[2:].strip() + '\n' for line in message[idxDirections:idxItems].split('\n') if line[:2] == '- ']

    return roomName, directions, items


def getItems(process, parent, fromDirection, visited):
    global badItems

    process.Interpret()

    message = printOutput(process)
    roomName, directions, availableItems = parseMessage(message)
    for item in availableItems:
        if item not in badItems:
            addInput(process, take([item]))
            process.Interpret()
            printOutput(process)

    if roomName not in visited:
        visited[roomName] = (parent, fromDirection)
        if roomName != 'Security Checkpoint':
            for direction in directions:
                if direction != fromDirection:
                    retDirection = aboutFace(direction)

                    addInput(process, direction)
                    getItems(process, roomName, retDirection, visited)
                    addInput(process, retDirection)
                    process.Interpret()
                    printOutput(process)
        else:
            visited['END'] = ('Security Checkpoint', fromDirection)


def parseInv(process):
    addInput(process, 'inv\n')
    process.Interpret()
    message = printOutput(process)

    idxItems = message.find('Items in your inventory:')
    if idxItems != -1:
        return [line[2:].strip() + '\n' for line in message[idxItems:].split('\n') if line[:2] == '- ']
    return []


def aboutFace(direction):
    if direction == N:
        return S
    elif direction == S:
        return N
    elif direction == E:
        return W
    elif direction == W:
        return E
    assert False


def day25a():
    program = loadProgram('day25.txt')
    process = Process(program)
    process.echoOutput = False
    process.echoInput = False

    # collect all the items (also builds a map)
    graph = {}
    getItems(process, None, None, graph)

    # pathfind to the security checkpoint
    path = []
    parent, direction = graph['Security Checkpoint']
    while parent is not None:
        direction = aboutFace(direction)
        path.append(direction)

        parent, direction = graph[parent]

    path.reverse()

    # walk to the security checkpoint
    addInput(process, ''.join(path))
    process.Interpret()
    printOutput(process)

    # drop all items
    items = parseInv(process)

    addInput(process, drop(items))
    process.Interpret()
    printOutput(process)

    # try every combination
    tryall(process, items)


if __name__ == '__main__':
    #explore()
    day25a()
