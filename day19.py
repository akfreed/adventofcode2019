from intcode import Process, loadProgram
from utility import Vector, render_map


def sample(program, pos: Vector):
    proc = Process(program)
    proc.echoInput = False
    proc.echoOutput = False
    proc.PushInput(pos.x)
    proc.PushInput(pos.y)
    ret = proc.Interpret()
    assert ret == True
    return proc.PopOutput()


def day19a():
    program = loadProgram('day19.txt')

    res = {}
    for y in range(0, 50):
        for x in range(0, 50):
            pos = Vector(x, y)
            res[pos] = sample(program, pos)

    count = sum(res.values())
    print(count)

    v2s = {
        0: '.',
        1: '#'}
    render_map(res, v2s, '.')


def day19b():
    program = loadProgram('day19.txt')

    posUpperRight = Vector(99, 0)
    posLowerLeft = Vector(0, 99)

    while True:
        sur = sample(program, posUpperRight)
        sll = sample(program, posLowerLeft)
        if sur == 0:
            posUpperRight = posUpperRight.down()
            posLowerLeft = posLowerLeft.down()
        if sll == 0:
            posUpperRight = posUpperRight.right()
            posLowerLeft = posLowerLeft.right()
        if sur == 1 and sll == 1:
            break

    print(Vector(posLowerLeft.x, posUpperRight.y))
    print(posLowerLeft.x * 10000 + posUpperRight.y)


if __name__ == '__main__':
    day19a()
    day19b()
